# Get Start

1. Install packages

```bash
$ npm install
```

2. Run server

```bash
$ node server.js
```

3. Open Browser with
- localhost:8080/api: You'll see "{"message":"welcome to our app !"}".

# API Table

| Method |                 URL               | Body Content  | Note                             |
| ------ | --------------------------------- | ------------- | -------------------------------- |
| GET    | localhost:8080/api/bears          | N/A           | Get all bears data.              |
| POST   | localhost:8080/api/bears          | name: 'Vicky' | Create a bear named Vicky.       |
| GET    | localhost:8080/api/bears/:bear_id | N/A           | Get a bear with specified id.     |
| PUT    | localhost:8080/api/bears/:bear_id | name: 'Nina'  | Update a bear with name Nina.    |
| DELETE | localhost:8080/api/bears/:bear_id | N/A           | Delete a bear with specified id.  |
 
