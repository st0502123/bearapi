// Base setup
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

var mongoose = require('mongoose');
mongoose.Promise = Promise;

mongoose.connect('mongodb://127.0.0.1:27017/nodejs', { useNewUrlParser: true });

var Bear = require('./app/models/bear');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;


// ROUTES
var router = express.Router();


// middleware
router.use(function(req, res, next){
	// do logging
	console.log('Something happens');
	next();
});



router.get('/', function(req, res){
	res.json({message: 'welcome to our app !'});
});


// More route for or api here

// routes that end in /bears
router.route('/bears')

	// create a bear (POST: localhost:8080/api/bears)
	.post(function(req, res){

		var bear = new Bear();
		bear.name = req.body.name;

		// save the bear
		bear.save()
			.then(()=>res.json({message: 'Bear created!'}))
			.catch(err => res.send(err));

		/*
		bear.save(function(err){
			if (err)
				res.send(err);
			res.json({message: 'Bear created!'});
		});
		*/
	})

	// get all bears (GET: localhost:8080/api/bears)
	.get(function(req, res){
		Bear.find()
			.then(bears => res.json(bears))
			.catch(err => res.send(err));

		/*
		Bear.find(function(req, res){
			if (err)	res.send(err);
			res.json(bears);
		});
		*/
	});

// Single Bear
router.route('/bears/:bear_id')

	// Get the bear with id (GET: http://localhost:8080/api/bears/:bear_id)
	.get(function(req, res){
		Bear.findById(req.params.bear_id)
			.then(bear => res.json(bear))
			.catch(err => res.send(err));

		/*
		Bear.findById(req.params.bear_id, function(err, bear){
			if(err)
				res.send(err);
			res.json(bear);
		});
		*/
	})

	// update the bear with id (PUT: http://localhost:8080/api/bears/:bear_id)
	.put(function(req, res){
		Bear.findById(req.params.bear_id)
			.then(bear => {
				bear.name = req.body.name;
				bear.save()
					.then(() => res.json({message: 'Bear updated!'}))
					.catch(err => res.send(err));
			})
			.catch(err => res.send(err));

		/*
		Bear.findById(req.params.bear_id, function(err, bear){
			if (err)	res.send(err);

			bear.name = req.body.name; // update

			// save bear
			bear.save(function(err){
				if (err) res.send(err);
				res.json({message: 'Bear updated!'});
			});
		});
		*/
	})

	// delete the bear with id (DELETE: http://localhost:8080/api/bears/:bear_id)
	.delete(function(req, res){
		Bear.remove({_id: req.params.bear_id,})
			.then(bear => res.json({message: 'Bear deleted!'}))
			.catch(err => res.send(err));

		/*
		Bear.remove({
			_id: req.params.bear_id
		}, function(err, bear) {
			if (err)	res.send(err);
			res.json({message: 'Bear deleted!'});
		})
		*/
	});


// REGISTER OUR ROUTES
// prefix with /api

app.use('/api', router);

// START SERVER
app.listen(port);
console.log('Magic on port ' + port);